package com.ericsson.rum.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ericsson.rum.adapter.ResourceAdapter;
import com.ericsson.rum.service.FetchResourcesService;
import com.ericsson.rum.support.DataProvider;
import com.ericsson.rum.support.NewDataListener;
import com.ericsson.rum.support.Resource;
import com.example.rum.rum.R;

import java.util.ArrayList;
import java.util.List;


public class ResourceViewCurrentUserFragment extends Fragment {

    List<Resource> resourceListAll = new ArrayList<>();
    ListView fragmentListView;
    ResourceAdapter resourceAdapter;
    TextView emptyList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        resourceListAll = filter(DataProvider.getResourceList());
        View inflatedView = inflater.inflate(R.layout.group_fragment, container, false);
        fragmentListView = inflatedView.findViewById(R.id.resource_listview);
        resourceAdapter = new ResourceAdapter(getContext(),
                R.layout.resource_view, resourceListAll);
        fragmentListView.setAdapter(resourceAdapter);
        fragmentListView.invalidateViews();
        fragmentListView.setClickable(true);
        emptyList = inflatedView.findViewById(R.id.empty_list);
        if (resourceListAll.isEmpty()){
            emptyList.setVisibility(View.VISIBLE);
        }
        fragmentListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView adapterView, View view, int i, long l) {
                final Resource resource = (Resource) adapterView.getItemAtPosition(i);
                final int iterator = i;
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                if (resource.getState() == 1) {
                    builder.setMessage(R.string.dialog)
                            .setCancelable(false)
                            .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d("CurrentFragment", "Booking id:" + resource.getId());
                                    Thread a = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            String response = FetchResourcesService.getResponseFromServer(DataProvider.getBookURL(DataProvider.getSessionID(), resource.getId()));
                                            FetchResourcesService.getAllData();
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {

                                                    Toast.makeText(getContext(), getText(R.string.reservation) + resource.getName(), Toast.LENGTH_SHORT).show();
                                                    resourceListAll = filter(DataProvider.getResourceList());
                                                    resourceAdapter.notifyDataSetChanged();
                                                }
                                            });
                                        }
                                    });
                                    a.start();


                                }
                            })
                            .setNegativeButton(R.string.decline, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Toast.makeText(getContext(), R.string.dismiss,
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.setTitle(R.string.dialog);
                    alert.setMessage(getText(R.string.question)+ resource.getName() +"?");
                    alert.show();
                } else {
                    builder.setMessage(R.string.dialog)
                            .setCancelable(false)
                            .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    Thread thread = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            String response = FetchResourcesService.getResponseFromServer(DataProvider.getReleaseURL(DataProvider.getSessionID(), resource.getId()));
                                            FetchResourcesService.getAllData();
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {

                                                    Toast.makeText(getContext(), getText(R.string.release), Toast.LENGTH_SHORT).show();
                                                    resourceListAll = filter(DataProvider.getResourceList());
                                                    resourceAdapter.notifyDataSetChanged();
                                                }
                                            });
                                        }
                                    });
                                    thread.start();


                                }
                            })
                            .setNegativeButton(R.string.decline, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Toast.makeText(getContext(), R.string.dismiss,
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.setTitle(R.string.dialog);
                    alert.setMessage(getText(R.string.question2)+ resource.getName() +"?");
                    alert.show();

                }
            }
        });
        fragmentListView.invalidateViews();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                FetchResourcesService.getAllData();
            }
        });
        thread.start();

        DataProvider.addNewDataListener("currentUserFragment",new NewDataListener() {
            @Override
            public void newDataEvent() {
                Activity main = getActivity();
                if(main!=null){
                    main.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("Curr", "NewDataEvent");
                            resourceAdapter.clear();
                            resourceAdapter.addAll(filter(DataProvider.getResourceList()));
                            resourceAdapter.notifyDataSetChanged();
                            if (resourceListAll.isEmpty()){
                                emptyList.setVisibility(View.VISIBLE);
                            }else{
                                emptyList.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            }
        });
        return inflatedView;
    }

    private List<Resource> filter(List<Resource> resourceList) {
        List<Resource> result = new ArrayList<>();
        for (int i = 0; i< resourceList.size(); i++){
            if (resourceList.get(i).getOwner().equals(DataProvider.getLogin())){
                result.add(resourceList.get(i));
            }
        }
        return result;
    }

}
