package com.ericsson.rum.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ericsson.rum.support.DataProvider;
import com.example.rum.rum.R;

public class LoginFragment extends Fragment {

    private Button signInButton;
    EditText loginField, passwordField;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View inflatedView = inflater.inflate(R.layout.fragment_login, container, false);

        signInButton = (Button) inflatedView.findViewById(R.id.sign_in_button_in_login);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResourcesFragment fragment = new ResourcesFragment();

                loginField = inflatedView.findViewById(R.id.login_field);
                passwordField = inflatedView.findViewById(R.id.password_field);
                String login = loginField.getText().toString();
                String password = passwordField.getText().toString();

                DataProvider.setLogin(login);
                DataProvider.setPassword(password);

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        // Inflate the layout for this fragment
        return inflatedView;
    }
}