package com.ericsson.rum.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.ericsson.rum.support.DataProvider;
import com.ericsson.rum.support.Resource;
import com.example.rum.rum.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResourcesFragment extends Fragment {

    private Button mineButton, availableButton, allButton, logoutButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inflatedView = inflater.inflate(R.layout.fragment_resources, container, false);
        ResourceViewAllResourcesFragment fragment = new ResourceViewAllResourcesFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container_resources, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        mineButton = inflatedView.findViewById(R.id.mine_button);
        mineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResourceViewCurrentUserFragment fragment = new ResourceViewCurrentUserFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container_resources, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        availableButton = inflatedView.findViewById(R.id.available_button);
        availableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResourceViewFreeResourcesFragment fragment = new ResourceViewFreeResourcesFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container_resources, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        allButton = inflatedView.findViewById(R.id.all_button);
        allButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResourceViewAllResourcesFragment fragment = new ResourceViewAllResourcesFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container_resources, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        logoutButton = inflatedView.findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataProvider.setLogin(null);
                DataProvider.setPassword(null);
                DataProvider.groups=new ArrayList<>();
                DataProvider.setSessionID(-1L);
                Fragment fragment = new LoginFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                Toast.makeText(getContext(),
                        R.string.loggedout, Toast.LENGTH_LONG).show();
            }
        });

        // Inflate the layout for this fragment
        return inflatedView;
    }




}
