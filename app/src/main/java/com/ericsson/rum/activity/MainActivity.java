package com.ericsson.rum.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.ericsson.rum.fragment.MainFragment;
import com.ericsson.rum.support.DataProvider;
import com.ericsson.rum.support.Resource;
import com.example.rum.rum.R;

import java.util.ArrayList;
import java.util.List;



public class MainActivity extends FragmentActivity {


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainFragment fragment = new MainFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();


    }
}
