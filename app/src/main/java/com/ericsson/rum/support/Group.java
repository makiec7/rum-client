package com.ericsson.rum.support;

import java.util.List;

public class Group {
    private int id;
    private String name;
    private List<Resource> resources;

    public Group(int id, String name, List<Resource> resources) {
        this.id = id;
        this.name = name;
        this.resources = resources;
    }

    public String getName() {
        return name;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public int getId() {
        return id;
    }
}