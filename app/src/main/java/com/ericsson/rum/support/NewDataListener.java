package com.ericsson.rum.support;

public interface NewDataListener {

    void newDataEvent();
}
