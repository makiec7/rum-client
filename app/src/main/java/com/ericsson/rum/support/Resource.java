package com.ericsson.rum.support;

public class Resource {
    private int id;
    private String name;
    private int state;
    private String description;
    private int groupId;
    private String owner;

    public Resource(int id, String name, int state, String description, int groupId, String owner) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.description = description;
        this.groupId = groupId;
        this.owner = owner;
    }

    public Resource(int id, String name, int state, String description, int groupId) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.description = description;
        this.groupId = groupId;
        this.owner = "";
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getState() {
        return state;
    }

    public String getDescription() {
        return description;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    public void setState(int i) {
        this.state = i;
    }
}