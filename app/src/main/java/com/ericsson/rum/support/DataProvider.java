package com.ericsson.rum.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataProvider {
    private static long sessionId = -1;
    private static final String serverAddress = "http://18.188.84.78:8080/rest/";
    private static String resourcesURL = serverAddress + "resources?";
    private static String loginURL = serverAddress + "login?";
    private static String bookURL = serverAddress + "bookResource?";
    private static String releaseURL = serverAddress + "releaseResource?";
    public static List<Group> groups;
    private static String login;
    private static String password;
    private static Map<String, NewDataListener> newDataListener;

    public static String getLogin() {
        return login;
    }

    public static String getPasswd() {
        return password;
    }

    public static void setPassword(String password) {
        DataProvider.password = password;
        DataProvider.sessionId = -1;
    }

    public static void setLogin(String login) {
        DataProvider.login = login;
    }

    public static boolean sessionIsActive() {
        return sessionId !=-1;
    }

    public static String getResourcesURL(String sessionID){
        return resourcesURL + createSessionParam(sessionID);
    }

    public static String getLoginURL(String login, String password){
        return loginURL + createLoginParam(login) + "&" + createPasswordParam(password);
    }

    private static String createSessionParam(String sessionID) {
        return "sessionID=" + sessionID;
    }
    private static String createLoginParam(String login) {
        return "login=" + login;
    }
    private static String createPasswordParam(String password) {
        return "passwd=" + password;
    }

    public static void setSessionID(long sessionID) {
        DataProvider.sessionId = sessionID;
    }

    public static String getSessionID() {
        return String.valueOf(DataProvider.sessionId);
    }

    public static void newDataEvent() {
        if(newDataListener!=null){
            for(NewDataListener currentListener:newDataListener.values())
                if(currentListener!=null)
                    currentListener.newDataEvent();
        }
    }

    public static void addNewDataListener(String name, NewDataListener dataListener){
        if(DataProvider.newDataListener==null)
            newDataListener = new HashMap<>();
        DataProvider.newDataListener.put(name, dataListener);
    }

    public static void addGroups(List<Group> groupsList) {
        groups = groupsList;
    }

    public static List<Resource> getResourceList() {
        List<Resource> all = new ArrayList<>();
        if(groups!=null) {
            List<Group> temp = new ArrayList<>(groups);
            for (Group current : temp) {
                all.addAll(current.getResources());
            }
        }
        return all;
    }

    public static String getBookURL(String sessionID, int resourceId) {
        return bookURL + createSessionParam(sessionID)+ "&" + createResourceParam(resourceId);
    }

    public static String getReleaseURL(String sessionID, int resourceId) {
        return releaseURL + createSessionParam(sessionID)+ "&" + createResourceParam(resourceId);
    }

    private static String createResourceParam(int resourceId) {
        return "resource_id=" + resourceId;
    }
}
