package com.ericsson.rum.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.ericsson.rum.support.DataProvider;
import com.ericsson.rum.support.Group;
import com.ericsson.rum.support.Resource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.ericsson.rum.support.DataProvider.groups;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class FetchResourcesService extends IntentService {

    public static final String NOTIFICATION = "com.example.rum.rum";
    private static final String TAG = "RUM_Service";

    public FetchResourcesService() {
        super("FetchResourcesService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String response = "";

        Log.d("RECEIVER", "group: " );




        int groupId = -1;

        try {
            JSONObject data = new JSONObject(response);
            data = data.getJSONObject("data");
            Iterator<String> keys = data.keys();

            while(keys.hasNext()) {
                List<Resource> resources = new ArrayList<>();

                String groupName = keys.next();

                JSONArray resourcesArray = data.getJSONArray(groupName);

                for(int i = 0; i < resourcesArray.length(); i++) {
                    JSONObject resourceJSON = resourcesArray.getJSONObject(i);

                    int id = resourceJSON.getInt("id");
                    String name = resourceJSON.getString("rName");
                    int state = resourceJSON.getInt("rState");
                    String description = resourceJSON.getString("rDescription");
                    groupId = resourceJSON.getInt("rGroupId");

                    Resource resource = new Resource(id, name, state, description, groupId);
                    resources.add(resource);
                }

                Group group = new Group(groupId, groupName, resources);
                groups.add(group);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent1 = new Intent(NOTIFICATION);
        DataProvider.groups = groups;
        sendBroadcast(intent1);
    }


    @Override
    public int onStartCommand(Intent intent,int flags,int startId) {
        Log.d(TAG,"Rum service started.");
        Toast.makeText(this, "Rum service started", Toast.LENGTH_LONG).show();
        String login = DataProvider.getLogin();
        String passwd = DataProvider.getPasswd();
        new Thread()
        {
            public void run() {
                 inBackground();
            }
        }.start();

        return START_STICKY;
    }

    private void inBackground() {
        while(true)
        {
            Log.d(TAG,"Checking data from server.");
            try {
                Thread.sleep(1000*10);
                // logowanie
                checkLogin();

                // pobieranie danych
                getAllData();


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public static void getAllData() {
        List<Group> groupsList = new ArrayList<>();
        if(DataProvider.sessionIsActive()){
            String response = getResponseFromServer(DataProvider.getResourcesURL(DataProvider.getSessionID()));
            Log.i("DJKASDADA", response);
            try {
                JSONObject data = new JSONObject(response);
                data = data.getJSONObject("data");
                Iterator<String> keys = data.keys();

                while(keys.hasNext()) {
                    List<Resource> resources = new ArrayList<>();
                    int groupId=-1;
                    String groupName = keys.next();

                    JSONArray resourcesArray = data.getJSONArray(groupName);


                    for(int i = 0; i < resourcesArray.length(); i++) {
                        JSONObject resourceJSON = resourcesArray.getJSONObject(i);
                        Resource resource = null;

                        int id = resourceJSON.getInt("id");
                        String name = resourceJSON.getString("rName");
                        int state = resourceJSON.getInt("rState");
                        String description = resourceJSON.getString("rDescription");
                        groupId = resourceJSON.getInt("rGroupId");
                        if (state == 0) {
                            String owner = resourceJSON.getString("user");
                            resource = new Resource(id, name, state, description, groupId, owner);
                        } else {
                            resource = new Resource(id, name, state, description, groupId);
                        }

                        resources.add(resource);
                    }
                    if (groupId>=0) {
                        Group group = new Group(groupId, groupName, resources);
                        groupsList.add(group);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //Intent intent1 = new Intent(NOTIFICATION);
            DataProvider.addGroups(groupsList);
            DataProvider.newDataEvent();
        }else
            Log.d(TAG, "User not logged.");
    }

    private void checkLogin() {
        if(!DataProvider.sessionIsActive()){
            signIn();
        }
    }

    private void signIn() {
        if(DataProvider.getLogin()!=null&&DataProvider.getPasswd()!=null) {
            String response = getResponseFromServer(DataProvider.getLoginURL(DataProvider.getLogin(), DataProvider.getPasswd()));
            if(response.contains("Password is incorrect")){
                //TODO:informacja do usera że jest zle hasło
            }else{
                try {
                    JSONObject data = new JSONObject(response);
                    long sessionID = data.getLong("sessionID");
                    DataProvider.setSessionID(sessionID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getResponseFromServer(String request) {
        String result="";
        try {
            //URL fetchResourcesURL = new URL(DataProvider.getResourcesURL + DataProvider.sessionId);
            URL fetchResourcesURL = new URL(request);
            HttpURLConnection urlConnection = (HttpURLConnection) fetchResourcesURL.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;

            while((line = reader.readLine()) != null) {
                result += line;
            }

            reader.close();
            urlConnection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }



}

/*

    Sprawdzać, czy obiekt MESSAGE tego jsona jest równy "OK" jeśli tak, to robić parsowanie
    Zrobić serwis, który obsluzy logowanie i ustawi sessionId w klasie DataProvider

    Dodatkowo w klasie, skąd będzie odpalany ten serwis powinny być następujące metody:

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(FetchResourcesService.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

        private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("RECEIVER", "message received");
        }
    };
 */