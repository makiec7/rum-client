package com.ericsson.rum.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ericsson.rum.support.DataProvider;
import com.ericsson.rum.support.Group;
import com.ericsson.rum.support.Resource;
import com.example.rum.rum.R;

import java.util.List;

public class ResourceAdapter extends ArrayAdapter<Resource> {

    private Context mContext;
    int mResource;

    public ResourceAdapter(Context context, int resource, List<Resource> objects) {
        super(context, resource, objects);
        this.mResource = resource;
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        int id = getItem(position).getId();
        String name = getItem(position).getName();
        int state = getItem(position).getState();
        String description= getItem(position).getDescription();
        int groupId = getItem(position).getGroupId();
        String owner = getItem(position).getOwner();
        String groupName = "";

        Resource resource = new Resource(id, name, state, description, groupId, owner);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        TextView resourceName = convertView.findViewById(R.id.resource_name);
        TextView resourceOwner = convertView.findViewById(R.id.resource_owner);
        TextView group =  convertView.findViewById(R.id.group);

        for (Group g: DataProvider.groups) {
            if (groupId == g.getId()){
                groupName = g.getName();
            }
        }
        resourceName.setText(name);
        resourceOwner.setText(owner);
        group.setText(groupName);



        if (state == 1){
            convertView.setBackgroundColor(Color.parseColor("#b3ffbb"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#ffa6a6"));
        }

        return convertView;
    }

}
